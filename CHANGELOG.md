## [1.0.0] - Initial Release.
## [1.0.1] - Fix asset loading
## [1.0.2] - Fix update password
## [1.0.3] - Fix update password and paginated view
## [3.2.5] - All dependent packages are updated and PaginatedListView is now more customizable.
## [3.3.0] - Updated user model to support user type
## [3.4.0] - Added client model

