import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class LoaderAnimation extends StatelessWidget {
  const LoaderAnimation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            height: 250,
            child: RiveAnimation.asset(
              'packages/techlify_core/assets/loader.riv',
            )));
  }
}
