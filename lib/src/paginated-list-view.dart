import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:techlify_core/techlify_core.dart' as core;
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

typedef BuilderWidget<T> = Widget Function(
    BuildContext context, T item, int index);
typedef Fetch = Function(int pageKey);

class PaginatedListView<T> extends StatefulWidget {
  PaginatedListView(
      {Key? key,
      required this.fetchData,
      this.physics,
      this.color = Colors.blueAccent,
      this.emptyMessage,
      required this.itemBuilder,
      this.noMoreItemsMessage,
      this.emptyWidget,
      this.loadingWidget,
      required this.controller})
      : super(key: key);
  final PagingController<int, T> controller;
  final Fetch fetchData;
  final BuilderWidget<T> itemBuilder;
  final String? emptyMessage;
  final String? noMoreItemsMessage;
  final Widget? emptyWidget;
  final Widget? loadingWidget;
  final ScrollPhysics? physics;
  final Color? color;
  @override
  _PaginatedListViewState<T> createState() => _PaginatedListViewState<T>();
}

class _PaginatedListViewState<T> extends State<PaginatedListView<T>> {
  @override
  void initState() {
    super.initState();
    widget.controller.addPageRequestListener((pageKey) async {
      List<T> items = await widget.fetchData(pageKey);
      paginate(items, pageKey);
    });
  }

  void updateList() {
    widget.controller.refresh();
  }

  void paginate(List<T> items, int pageKey, {bool isCache = false}) {
    bool isLastPage = items.length < 25;
    if (!isLastPage) {
      if (isCache) {
        widget.controller.appendPage(items, pageKey);
      }
      final nextPageKey = ++pageKey;
      widget.controller.appendPage(items, nextPageKey);
    } else {
      widget.controller.appendLastPage(items);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PagedListView<int, T>(
      shrinkWrap: true,
      physics: widget.physics,
      pagingController: widget.controller,
      builderDelegate: PagedChildBuilderDelegate<T>(
        noMoreItemsIndicatorBuilder: (context) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
              child: Text(widget.noMoreItemsMessage ?? '..That\'s all..',
                  style: TextStyle(color: Colors.grey, fontSize: 12))),
        ),
        itemBuilder: (context, item, index) =>
            widget.itemBuilder(context, item, index),
        firstPageErrorIndicatorBuilder: (context) => core.EmptyList(
          message: widget.controller.error,
        ),
        newPageProgressIndicatorBuilder: (context) => Center(
          child: SpinKitThreeBounce(
            color: widget.color,
          ),
        ),
        firstPageProgressIndicatorBuilder: (context) =>
            widget.loadingWidget ?? core.LoaderAnimation(),
        animateTransitions: true,
        noItemsFoundIndicatorBuilder: (context) =>
            widget.emptyWidget ??
            core.EmptyList(
              message: widget.emptyMessage,
            ),
      ),
    );
  }
}
