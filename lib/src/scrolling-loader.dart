import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class ScrollingLoaderAnimation extends StatelessWidget {
  const ScrollingLoaderAnimation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            height: 120,
            child: RiveAnimation.asset(
              'packages/techlify_core/assets/elips-loader.riv',
              artboard: 'One_more_loader',
            )));
  }
}
