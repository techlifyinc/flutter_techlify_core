import 'package:flutter/material.dart';
import 'package:techlify_core/techlify_core.dart';
import 'package:reactive_date_time_picker/reactive_date_time_picker.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:reactive_dropdown_search/reactive_dropdown_search.dart';

typedef LabelBuilder<T> = String Function(T item);

void onLoading(BuildContext context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Container(
        color: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spinner(),
          ],
        ),
      );
    },
  );
}

Widget textForm(String control, String label, String validation,
    {TextInputType? keyboardType,
    IconData? suffixIcon,
    bool isPassword = false}) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 6.0),
    child: ReactiveTextField<String>(
      obscureText: isPassword,
      keyboardType: keyboardType,
      maxLines: keyboardType == TextInputType.multiline ? 4 : 1,
      formControlName: control,
      decoration: inputDecoration(label,
          suffixIcon: suffixIcon,
          isMultiLine: keyboardType == TextInputType.multiline),
      validationMessages: {
        ValidationMessage.required: (errors) => '$validation is required.',
        ValidationMessage.email: (errors) => 'Invalid email address'
      },
    ),
  );
}

Widget dateForm(String control, String label, String validation,
    {bool isDateTime = false, IconData? suffixIcon}) {
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 6.0),
    child: ReactiveDateTimePicker(
      formControlName: control,
      firstDate: DateTime(1985),
      lastDate: DateTime(2030),
      decoration: inputDecoration(label, suffixIcon: suffixIcon),
      type: isDateTime
          ? ReactiveDatePickerFieldType.dateTime
          : ReactiveDatePickerFieldType.date,
      validationMessages: {
        ValidationMessage.required: (errors) => '$validation is required.',
      },
    ),
  );
}

class SingleSelectDropdown<T> extends StatelessWidget {
  const SingleSelectDropdown(
      {Key? key,
      required this.control,
      required this.label,
      required this.labelBuilder,
      required this.api,
      required this.modelFromJson,
      this.params})
      : super(key: key);
  final String control;
  final String label;
  final LabelBuilder<T> labelBuilder;
  final TfromJson modelFromJson;
  final String api;
  final Map<String, String>? params;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ReactiveDropdownSearch(
        formControlName: control,
        dropdownDecoratorProps: DropDownDecoratorProps(
            dropdownSearchDecoration: inputDecoration(label)),
        itemAsString: labelBuilder,
        asyncItems: getData,
        showClearButton: true,
      ),
    );
  }

  Future<List<T>> getData(String query) async {
    var data = await DataService.get(api, params: params);
    var models = [];
    if (data != null) {
      models =
          List<T>.from((data['data'] as List).map((x) => modelFromJson(x)));
    }
    return [...models];
  }
}
