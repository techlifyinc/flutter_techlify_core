import 'package:techlify_core/src/models/client.model.dart';

class User {
  int? id;
  String? name;
  String? email;
  int? userTypeId;
  int? clientId;
  int? isEnabled;
  Client? client;

  User({
    this.id,
    this.name,
    this.email,
    this.userTypeId,
    this.clientId,
    this.isEnabled,
    this.client,
  });

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return parsedJson.isNotEmpty
        ? User(
            id: parsedJson['id'],
            name: parsedJson['name'],
            email: parsedJson['email'],
            userTypeId: parsedJson['user_type_id'],
            clientId: parsedJson['client_id'],
            isEnabled: parsedJson['enabled'],
            client: parsedJson['client'] != null
                ? Client.fromJson(parsedJson['client'])
                : null, // Handling the client field here
          )
        : User();
  }

  Map<String, dynamic> toJson() {
    final map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['user_type_id'] = userTypeId;
    map['client_id'] = clientId;
    map['enabled'] = isEnabled;
    map['client'] =
        client != null ? client!.toJson() : null; // Convert client back to JSON

    return map;
  }
}
