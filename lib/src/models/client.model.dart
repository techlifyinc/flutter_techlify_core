class Client {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? address;
  int? creatorId;
  int? status_id;
  String? logo;

  Client({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.address,
    this.creatorId,
    this.status_id,
    this.logo,
  });

  factory Client.fromJson(Map<String, dynamic> parsedJson) {
    return parsedJson.isNotEmpty
        ? Client(
            id: parsedJson['id'],
            name: parsedJson['name'],
            email: parsedJson['email'],
            phone: parsedJson['phone'],
            address: parsedJson['address'],
            creatorId: parsedJson['creator_id'],
            status_id: parsedJson['status_id'],
            logo: parsedJson['logo'],
          )
        : Client();
  }

  Map<String, dynamic> toJson() {
    final map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['phone'] = phone;
    map['address'] = address;
    map['creator_id'] = creatorId;
    map['status_id'] = status_id;
    map['logo'] = logo;

    return map;
  }
}
