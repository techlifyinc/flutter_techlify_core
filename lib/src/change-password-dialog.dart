import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../techlify_core.dart';

class ChangePasswordDialog {
  ChangePasswordDialog({
    required this.context,
  }) {
    changePassword(context);
  }
  final BuildContext context;

  BlurDialog changePassword(context) {
    return BlurDialog(
        container: ChangePasswordWidget(
          context: context,
        ),
        context: context);
  }
}

class ChangePasswordWidget extends StatefulWidget {
  const ChangePasswordWidget({Key? key, required this.context})
      : super(key: key);
  final BuildContext context;
  @override
  State<ChangePasswordWidget> createState() => _ChangePasswordWidgetState();
}

class _ChangePasswordWidgetState extends State<ChangePasswordWidget> {
  String? _currentPassword;
  String? _updatedPassword;

  var _form = GlobalKey<FormState>();

  bool validateAndSaveForm() {
    var form = _form.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> updatePassword() async {
    if (validateAndSaveForm()) {
      final http.Response response = await http.patch(
        Uri.https(
          Config().readValue('coreDomain'),
          Config().readValue('baseUri') + 'api/user/current/update-password',
        ),
        body: json.encode({
          "current_password": _currentPassword,
          "password": _updatedPassword,
          "password_confirmation": _updatedPassword,
        }),
        headers: {
          ...await DataService.getHeaders(),
          HttpHeaders.acceptHeader: "application/json",
        },
      );
      final responseData = json.decode(response.body);
      if (response.statusCode == 200) {
        ToastMessage(message: "Password Updated");
      } else {
        ToastMessage(
            message: responseData['message'] ==
                    "validation.password.uncompromised"
                ? "According to Google's Risky Password Database, your new password is compromised publicly and is weak. This cannot be used"
                : responseData['message']);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
        width: size.width * 0.8,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _form,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () => Navigator.pop(widget.context),
                    ),
                    SizedBox(width: 10),
                    Text(
                      'Update Your Password',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                    ),
                  ]),
                  SizedBox(height: 15),
                  TextFormField(
                    obscureText: true,
                    onChanged: (value) {
                      _currentPassword = value;
                    },
                    validator: (value) {
                      if (value != null) return null;

                      return 'Current Password can\'t be empty';
                    },
                    decoration: inputDecoration('Current Password',
                        prefixIcon: Icons.lock_open),
                  ),
                  SizedBox(height: 10),
                  TextFormField(
                      obscureText: true,
                      onChanged: (value) {
                        _updatedPassword = value;
                      },
                      validator: (value) {
                        if (value != null) return null;

                        return 'New Password can\'t be empty';
                      },
                      decoration: inputDecoration('New Password',
                          prefixIcon: Icons.lock)),
                  SizedBox(height: 10),
                  SubmitButton(
                      onPressed: () async {
                        FocusScope.of(widget.context).requestFocus(FocusNode());
                        await updatePassword();
                      },
                      text: 'Change Password'),
                ]),
          ),
        ));
  }
}
